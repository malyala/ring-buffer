##Ring buffer

This is a ring buffer project to write bytes to a array of fixed length and read from it. The program reads as many bytes which are written 
to the buffer which is desirable in asynchronous writes and reads type scenarios. This Program is highly customizable and can be modified to 
write and read data of any type.A std::vector can also be used in place of a array.

