/*
Name :  RingBuffer.cpp
Author: Amit Malyala 
Version: 0.1 Initial version 
License:
Copyright <2018> <Amit Malyala>

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the 
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
Description:
A ring buffer of fixed length to fill and retrieve char bytes.This can also be changed to a ring buffer which uses integers.
The program should read as many elements which are written previously.
Example:
4 writes, 4 reads
9 writes, 9 reads.
2 writes, 2 reads.
10 writes, 10 reads.
*/
#include "RingBuffer.h"
#include <iostream>
#include <chrono>

int main(void) 
{
  CalculateRingBufferThroughput();
  return 0;
}
/* 
Component function: void  CalculateRingBufferThroughput(void)
Arguments: none
Description: Calculates Ringbuffer Throughput.
*/
void CalculateRingBufferThroughput(void)
{
    char a = 'x';
    // Measure function execution time.
    auto start = std::chrono::steady_clock::now();
    RingBuffer RingBuffer;
    RingBuffer.InitQueue();
    auto end = std::chrono::steady_clock::now();
    auto diff = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
    std::cout << "Initialization time of a ring buffer: " << diff << " nanoseconds" << std::endl;
    // Measure function execution time.
    start = std::chrono::steady_clock::now();
    for (long long int x=0;x<8388608;x++)
    {
	/* Writing to queue, writes should be less than length */
	for (a=97;a<107;a++)
	{
        	RingBuffer.Writequeue(a);
	}
	/* This function reads as many entries written into ReadBuffer[] */
	RingBuffer.ReadQueue();
    }
    // Measure execution time.
    end = std::chrono::steady_clock::now();
    diff = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    std::cout << "Execution time for filling and reading 80 MB in a ring buffer: " << diff << " milliseconds";
}

/* 
Member function RingBuffer::RingBuffer()
Arguments: none
Description: Constructor.
*/
RingBuffer::RingBuffer()
{
    WritePosition=ReadPosition=0;
    Length=10;
}
/* 
Member function RingBuffer::~RingBuffer()
Arguments: none
Description: Destructor.
*/
RingBuffer::~RingBuffer()
{
	 
}
/* 
Member function: void RingBuffer::InitQueue(void)
Arguments: None
Description: 
Initializes queue 
*/
void RingBuffer::InitQueue(void)
{
    unsigned int i=0;
    while (i<Length)
    {
	WriteBuffer[i] = '0';
	ReadBuffer[i]='0';
	i++;
    }
    NumberofReads=0;
}
/* 
Member function: RingBuffer::Writequeue(char Data)
Arguments: Data to be written
Description: 
Writes to queue 
*/
void RingBuffer::Writequeue(char Data)
{
    WriteBuffer[WritePosition]=Data;
    WritePosition++;
    if (WritePosition == Length)
    {
        WritePosition=0;
    }
}
/* 
Member function: void RingBuffer::Readqueue(void)
Arguments: None
Description: 
Reads from queue and returns value read. 
Function being designed. 
*/
void RingBuffer::ReadQueue(void)
{
    unsigned int i=0;
    if (ReadPosition<WritePosition)
    {
	for (i=0;ReadPosition<WritePosition;i++, ReadPosition++)
	{
	    ReadBuffer[i]=WriteBuffer[ReadPosition];			
	}
	NumberofReads=i+1;
    }
    else
    {
	NumberofReads= (Length-ReadPosition+WritePosition);
	for (i=0;i<NumberofReads;i++)
	{
	    ReadBuffer[i]=WriteBuffer[ReadPosition];
	    ReadPosition++;
        if (ReadPosition==Length)
	    {
		ReadPosition=0;
	    }
	}
    }
}
/* 
Member function void RingBuffer::PrintReadBuffer(void)
Arguments: None
Description: 
Prints read buffer. 
*/
void RingBuffer::PrintReadBuffer(void)
{
    for(unsigned int i=0;i<NumberofReads;i++)
    {
	std::cout << ReadBuffer[i] << " ";
    }
    std::cout << std::endl;
}