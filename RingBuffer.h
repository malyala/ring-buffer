/*
Name : RingBuffer.h
Author: Amit Malyala 
License:
Copyright <2018> <Amit Malyala>

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the 
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Description:
A ring buffer of fixed length to fill and retrieve char bytes.This can also be changed to a ring buffer which uses integers.
To do:
Version: 
0.1 Initial version 
*/
#ifndef RINGBUFFER
#define RINGBUFFER
/* 
Class to implement a circular queue or ring buffer.
*/
class RingBuffer
{
    private:

    /* Use arrays or vectors if length of buffer is fixed */
	char WriteBuffer[10];
	char ReadBuffer[10];
	/* Store current read and write positions */
	unsigned int WritePosition;
	unsigned int ReadPosition;
	/* Length of queue */
	unsigned int Length;
	unsigned int NumberofReads;
    public:	
	/* Constructor */
	RingBuffer();
	/* Destructor */
	~RingBuffer();
	/* Initialize queue */
	void InitQueue(void);
	/* Write to queue */
	void Writequeue(char Data);
	/* Read from queue into ReadBuffer*/
	void ReadQueue(void);
	/* 
	Member function: PrintReadBuffer(void)
	Arguments: None
	Description: 
	Prints read buffer. 
	*/
	void PrintReadBuffer(void);
};
/* 
Component function: void  CalculateRingBufferThroughput(void)
Arguments: none
Description: Calculates Ringbuffer Throughput.
*/
void CalculateRingBufferThroughput(void);
#endif